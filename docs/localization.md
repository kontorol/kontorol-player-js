# Localization

This article explains how to upload translation dictionaries to Kontorol platform,
and how to control the language that actually will be served to the player.

> This feature uses the `ui.translations` config, for more details see [Adding translations and choosing locale language](https://github.com/kontorol/pakhshkit-js-ui/blob/master/docs/translations.md).

## Uploading translation dictionaries via Kontorol platform

Plugin which served by Kontorol platform (via the bundle builder) can upload its translation dictionaries also by Kontorol platform.
To get this ability, The plugin needs to add the translation json file/s (`*.i18n.json`) under a `translations` folder in the location of the plugin source code and source map.
For example: Plugin called _pakhshkit-ui-plugin_, which contains, in the dist folder, 2 files - `pakhshkit-ui-plugin.js` and `pakhshkit-ui-plugin.js.map`,
Will add a `translations` folder with the translation file/s:
![](images/translation-tree.png)
Each json file contains the translation dictionary under the language code key.
For example `en.i18n.json` looks like:

```json
{
  "en": {
    "buttons": {
      "btn1": "Button1",
      "btn2": "Button2"
    }
  }
}
```

`fr.i18n.json` looks like:

```json
{
  "fr": {
    "buttons": {
      "btn1": "Bouton1",
      "btn2": "Bouton2"
    }
  }
}
```

Actually, the plugin could supply only one json contains multi language dictionaries. For example `langs.i18n.json` looks like:

```json
{
  "en": {
    "buttons": {
      "btn1": "Button1",
      "btn2": "Button2"
    }
  },
  "fr": {
    "buttons": {
      "btn1": "Bouton1",
      "btn2": "Bouton2"
    }
  }
}
```

## Control the served languages

By default only English dictionary injected to the player (by [`ui.translations`](https://github.com/kontorol/pakhshkit-js-ui/blob/master/docs/configuration.md#configtranslations) config).
There are 2 ways to change this behavior:

1.  By the Studio.
2.  By a Url parameter called `langs` on the embed player Url. For example:
    _https://qa-apache-php7.dev.kontorol.com/p/1091/sp/109100/embedPakhshkitJs/uiconf_id/15215933/partner_id/1091/langs/de,fr_.
