// @flow
import PolyfillManager from './common/polyfills/polyfill-manager';
import './common/polyfills/all';
// Import core
import * as core from '@pakhshkit-js/pakhshkit-js';
// Import ui
import * as ui from '@pakhshkit-js/pakhshkit-js-ui';
// Import provider
import * as providers from 'pakhshkit-js-providers';
// Import media source adapters
import '@pakhshkit-js/pakhshkit-js-hls';
import '@pakhshkit-js/pakhshkit-js-dash';
// Import setup method
import {setup} from './setup';
import {getPlayers, getPlayer} from './proxy';
// Import cast framework
import {cast} from './common/cast';
// Import playlist
import {playlist} from './common/playlist';

// Import plugin framework
import {Ad, AdBreak} from './common/ads';
import {BasePlugin, registerPlugin} from './common/plugins';

declare var __VERSION__: string;
declare var __NAME__: string;
declare var __PACKAGE_URL__: string;
declare var __PLAYER_TYPE__: string;

const PLAYER_NAME = __NAME__;
const PLAYER_TYPE = __PLAYER_TYPE__;
const VERSION = __VERSION__;

PolyfillManager.installAll();

/* eslint-disable no-import-assign */
core.Ad = Ad;
core.AdBreak = AdBreak;
core.BasePlugin = BasePlugin;
core.registerPlugin = registerPlugin;

export {
  getPlayers,
  getPlayer,
  core,
  ui,
  providers,
  setup,
  cast,
  playlist,
  Ad,
  AdBreak,
  BasePlugin,
  registerPlugin,
  PLAYER_TYPE,
  VERSION,
  PLAYER_NAME
};
