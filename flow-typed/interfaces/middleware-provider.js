// @flow
import {BaseMiddleware} from '@pakhshkit-js/pakhshkit-js';

declare interface IMiddlewareProvider {
  getMiddlewareImpl(): BaseMiddleware;
}
